---
module-name: [module-name-lower-case-dashes-for-spaces eg:] "gitlab-pages"
area: [one of:] "Customer Service" or "Core Technologies" or "Product Knowledge" or "Troubleshooting & Diagnostics"
gitlab-group: [name of gitlab stage:group for Product Knowledge modules from [this page](https://about.gitlab.com/handbook/product/categories/features/) e.g:] "Verify:Continuous Integration"
maintainers:
  - [GitLab username of module maintainer]
  - [optional: GitLab username of additional maintainer]
---

### Overview

**Goal**: <general goal of module>

*Length*: <estimate # of hours, use single number or range>

**Objectives**: At the end of this module, you should be able to:
- <insert a few concrete actions that learner should be able to do at the end of this module>

<any special instructions, like the order of stages>

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: <module title> - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Commit to this by updating your [knowledge_areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 1: <section name>

1. [ ] Task with any relevant link

### Stage X: Tickets

- [ ] **Done with Stage X**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage X: Pair on Customer Calls

Note: This stage may be optional where calls dedicated to the topic are uncommon.

- [ ] **Done with Stage X**

1. [ ] Pair on two diagnostic calls, where a customer is having trouble with GitLab <feature>.
   1. [ ] call with ___
   1. [ ] call with ___

### Stage X: Assessment

Note: Please do not look at the assessment until you are ready to complete it.

- [ ] **Done with Stage X**

1. [ ] Complete [the assessment](). Assessments are stored in the [Support Team Drive Training/Training Module Assessments folder](https://drive.google.com/drive/u/0/folders/147D5ecSDsV2J9OCN_6t-J4vYzPLTebue).
  - If you are linked to a Google doc, please make a copy, answer the questions, then share with your trainer or an expert when complete. They will assess your answers and give you feedback.
  - If you are linked to a Google form, please complete the self-assessment. If you have any questions about the answers, please ask your trainer or an expert.

### Penultimate Stage: Review

You feel that you can now do all of the objectives:

- [ ]  <same list as in the Overview section>

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself!

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your trainer review this issue. If you do not have a training, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic under your list of completed `modules`.

/label ~module
