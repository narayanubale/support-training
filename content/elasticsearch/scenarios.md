# Example scenarios

> I currently run a GitLab instance that has 1000 users and 10000 projects. I am
> using the ElasticSearch integration on my instance. I need to re-index the
> entire thing due to some issues we face on the ElasticSearch side. I need to
> avoid downtime and keep the GitLab instance performant during this process.
> How could I go about this?

> One of my devs did something screwy on the backend and now I need to re-index
> one of my projects. I don't want to re-index the entire thing. What do I do?

> I recently did a major update on GitLab and now none of my search data is
> updating. Everything seems to be broken there. Why?

> I'm trying to index my instance, but I'm seeing `No Elasticsearch Node
> available` errors. How can I address these errors and index my instance?


> When indexing my instance, I see
> `Elasticsearch::Transport::Transport::Errors::RequestEntityTooLarge` errors in
> the Sidekiq logs. How can I fix that error?


> I started my Elasticsearch index a few hours ago, and my search isn't
> performing as expected. There are incomplete results. How can I find out
> what's going on with my search?