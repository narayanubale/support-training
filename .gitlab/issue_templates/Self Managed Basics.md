---
module-name: "Self Managed Basics"
area: "Customer Service"
maintainers:
  - TBD
---

## Introduction

This checklist provides Support Engineers with the basics of answering Self-managed product-related tickets.

**Goals of this checklist**

At the end of this module, you should be able to:
- answer self-managed tickets, including asking for appropriate logs or other diagnostics.
- schedule and participate in calls with customers.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the modules under the Self Managed Support Basics Pathway.
- This issue should take you **2 weeks to complete**.

Reminders:

- **Public** issue: Don't include anything confidential.

### Stage 0: Create Your module

1. [ ] Create an issue using this template by making the Issue Title: <module title> - <your name>
1. [ ] Add yourself (and your trainer) as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 1. Gathering Diagnostics

- [ ] **Done with Stage 1**

**Goal** Understand the gathering of diagnostics for GitLab instances

1. [ ] Learn about the GitLab checks that are available
    1. [ ] [Environment Information and maintenance checks](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html)
    1. [ ] [GitLab check](https://docs.gitlab.com/ee/administration/raketasks/check.html)
    1. [ ] Omnibus commands
        1. [ ] [Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
        1. [ ] [Starting and stopping services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
        1. [ ] [Starting a rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-a-rails-console-session)

1. [ ] Learn about additional Support Tools
    1. [ ] [GitLab SOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos)
        1. [SOS Analyzer](https://gitlab.com/gitlab-com/support/toolbox/sos-analyzer)
        1. [SOS For K8S](https://gitlab.com/gitlab-com/support/toolbox/kubesos)
    1. [ ] [Fast-Stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats)
    1. [ ] [strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser)

### Stage 2. Customer Calls

- [ ] **Done with Stage 2**

For Application focused Support Engineers, you will want to focus on calls related to Application issues,
but you should also shadow at least 1 Solutions focused call, and vice-versa for Solutions focused team members.

Look at the `GitLab Support` Google Calendar to find customer calls to observe. Contact the person leading the call to check if it is okay for you to jump in on the call, and if they could stay on with you for a few minutes after the call, so you can ask them a few questions about the things you didn't understand. Also, ask them to ask you a few questions to make sure you understand the points they want to highlight from the call.

1. [ ] Read about [customer calls](https://about.gitlab.com/handbook/support/workflows/customer_calls.html).
    - Note: Most of our calls will be via Zoom, but some customers cannot use Zoom. We have [Cisco WebEx](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#webex) as an alternative.
1. [ ] Start arranging to pair on calls with other Support Engineers. Aim to cover at least two of each type of call (scheduled and unscheduled). Comment on this issue with the type of call you were in, who it was with, and the link to the relevant ticket (if one exists).
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___
   - Reverse Shadowing _(**You** lead calls with a GitLab Support Engineer giving you feedback and support)_
      - Once you have been added to the Group Round Robin (see below) or start getting requests for customers to perform support calls, invite other support team members to shadow your calls.
      - Aim for 2-5 of these reverse shadowing calls; keep going if you still feel uncomfortable, or if the topic looks challenging.
      - Good places to find team members to shadow your calls include asking your onboarding buddy, asking in the self-managed slack channel, or look in the [Support Team Knowledge Areas](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html) page of the handbook for people who may know more about the topic of the call.
<br>

1. [ ] Learn about how [customers can request upgrade assistance](https://about.gitlab.com/handbook/support/workflows/live-upgrade-assistance.html).
1. [ ] In rare cases, you may need to help a customer [patch an instance](https://about.gitlab.com/handbook/support/workflows/patching_an_instance.html).

**Getting Added to the Group Round Robin Calendly (check with your manager first)**

1. [ ] Request access to the [GitLab Support group account](https://about.gitlab.com/handbook/support/workflows/calendly.html#gitlab-support-group-account) by [opening a Individual Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) and assigning to your manager
1. [ ] Read our [Calendly page](https://about.gitlab.com/handbook/support/workflows/calendly.html) and change it from the GitLab setup to how Support use it, including:
    1. [ ] Set up Zoom integration on your Calendly account by going to [calendly integrations](https://calendly.com/integrations) and following the instructions for Zoom (make sure to [update the calendly event's location to use "Zoom"](https://help.calendly.com/hc/en-us/articles/360010008093-Zoom))
    1. [ ] Set the title of your personal calendly customer event to include 'support' so the zap works.
    1. [ ] Add a required question to your personal calendly customer event to ask the customer to enter the Zendesk ticket number.
    1. [ ] Install the browser plugin.
1. [ ] Request to be included in the [round-robin support call](https://about.gitlab.com/handbook/support/workflows/calendly.html#self-managed-round-robin) by [opening a Calendly Support Call Inclusion Request](https://gitlab.com/gitlab-com/support/support-ops/calendly/-/issues/new?issuable_template=Provisioning%20Request) and specifying the hours you'll be available in UTC relative to your timezone in both Standard Time and Daylight Saving Time (if applicable) and your [tz database designation](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) for future automation.

## Congratulations! You made it, and can now help customers with self-managed product-related tickets.

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~module
