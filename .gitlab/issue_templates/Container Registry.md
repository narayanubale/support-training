---
module-name: "Container Registry"
area: "Product Knowledge"
gitlab-group: "Package:Package"
maintainers:
  - TBD
---

Title: "GitLab Container Registry - your-name"

## Goal of this checklist

> Set a clear path for GitLab Container Registry Expert training

## Objectives:

* Learn about GitLab Container Registry and how to utilize it.
* Learn some of the more complex tasks of using the Container Registry.

---

### Stage 1: Commit and become familiar with what GitLab Container Registry is

* [ ] **Done with Stage 1**

  1. [ ] Ping your manager on the issue to notify them you have started.
  1. [ ] In your Slack Notification Settings, set **Container Registry** as  Highlight Words.
  1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
  1. [ ] Commit to this by notifying the current experts that they can start
         routing non-technical Container Registry questions to you.
  1. [ ] Learn about GitLab Container Regsitry
     1. [ ] Read [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
     User documentation
     1. [ ] Read [ GitLab Container Registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html)
     Administration documentation.
     1. [ ] Understand the difference between [GitLab's Registry fork](https://gitlab.com/gitlab-org/container-registry) and the [upstream project](https://github.com/docker-archive/docker-registry)

### Stage 2: Technical setup and exploring

* [ ] **Done with Stage 2**

  1. [ ] Setup a self-managed instance to use GitLab Container Registry. Ensure you try with:
     1. [ ] [Changing the registry's internal port](https://docs.gitlab.com/ee/administration/packages/container_registry.html#change-the-registrys-internal-port)
     1. [ ] Using both self-signed and Let's Encrypt SSLs
  1. [ ] Create a project 
     1. [ ] [Build and push an image from your local machine](https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-images-from-your-local-machine)
     1. [ ] [Build and push an image using GitLab CI/CD](https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-images-using-gitlab-cicd)
     1. [ ] Set up an [External Container Registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html#use-an-external-container-registry-with-gitlab-as-an-auth-endpoint)
     1. [ ] Explore the [Container Registry API](https://docs.gitlab.com/ee/api/container_registry.html) and try out [bulk deleting tags](https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository-tags-in-bulk)
     1. [ ] Learn about the [`gitlab-ctl registry-garbage-collect` command](https://docs.gitlab.com/ee/administration/packages/container_registry.html#recycling-unused-tags)
     1. [ ] Understand how docker registry authentication works by reading the [registry](https://docs.gitlab.com/ee/administration/logs.html#registry-logs) and GitLab logs. Also refer to https://docs.docker.com/registry/spec/auth/token/

### Stage 3: Quiz

- [ ] **Done with Stage 3**

   1. [ ] Schedule a call with a current Container Registry Expert. During this call, you
          will guide them through the following:
      1. [ ] Using a self-managed instance with a self-signed certificate([steps for becoming your own CA](https://stackoverflow.com/a/60516812)):
        1. [ ] Build a simple docker file and push it to the registry:
            
            On a local machine, create a file called `Dockerfile` and add the following content:
           
            ```docker
            FROM ubuntu:latest

            RUN apt update

            EXPOSE 8080
            ```

            Then authenticate against the registry and build, then push the image:
            [Reference the docs](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticating-to-the-gitlab-container-registry)

            ```bash
            docker login registry.example.com

            docker build -t registry.example.com/group/project/image .

            docker push registry.example.com/group/project/image
            ```

            Be sure to get the [Docker client to trust the self-signed certificate](https://docs.docker.com/registry/insecure/#use-self-signed-certificates)

   1. [ ] Once you have completed this, have the expert comment below
          acknowledging your success.

### Final Stage:

  1. [ ] Your Manager needs to check this box to acknowledge that you finished.
  1. [ ] Send a MR to declare yourself a GitLab Container Registry Expert on the team page.
  1. [ ] If you feel that something is not clear or missing in this module, please create MRs to update it.

/label module
