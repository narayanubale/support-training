---
module-name: "GitLab API"
area: "Product Knowledge"
gitlab-group: "Create:Ecosystem"
maintainers:
  - TBD
---

**Title:** _"GitLab API - **your-name**"_

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

**Goal of this checklist:** Set a clear path for GitLab API training

### Stage 1: Commit and Become familiar with what GitLab API is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical API related questions to you
1. [ ] Read the [GitLab API Overview](https://docs.gitlab.com/ee/api/README.html)

1. [ ] Find answers to and understand the following questions:
   1. [ ] How are the API routes handled differently from the Rails routes?
   1. [ ] What does it mean to [Grapify the API](http://www.ruby-grape.org/)?
   1. [ ] Where in the code do I look to find which attributes are exposed for an API route (e.g. users)?

### Stage 2: Using the GitLab API

- [ ] **Done with Stage 2**

Remember to contribute to any documentation that needs updating.

1. [ ] Read documentation for 5 different APIs, e.g. for projects, branches,
etc. Paste the links here.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Call the API using all the different authentication methods available
   1. [ ] [OAuth 2 Tokens](https://docs.gitlab.com/ee/api/README.html#oauth2-tokens)
   1. [ ] [Personal Access Tokens](https://docs.gitlab.com/ee/api/README.html#personal-access-tokens)
   1. [ ] [Session Cookie](https://docs.gitlab.com/ee/api/README.html#session-cookie)

1. [ ] Take a look at GitLab's [GraphQL explorer](https://gitlab.com/-/graphql-explorer). Using the docs (in left sidebar), build the following queries:
   1. [ ] List projects for a specific group (such as gitlab-com/support).
   1. [ ] List pipelines for a specific project (choose one from the previous query that has more than 100 pipelines).
   1. [ ] List the 2nd set of pipelines (since the previous query will be cut off at the first 100).
   1. [ ] Take a look at ticket [#134208](https://gitlab.zendesk.com/agent/tickets/134208) to check your answers.

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Look for 10 old API tickets and read through them to understand what the
issues were and how they were addressed. Paste the links here.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Look for 5 API related issues in the [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues) issue tracker and
paste the links here.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Answer 10 API tickets and paste the links here. Do this even if a ticket
seems too advanced for you to answer. Find the answers from an expert and relay
them to the customers.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Optional: Pair on two diagnostic calls, where a customer is having trouble with GitLab API.
   1. [ ] call with ___
   1. [ ] call with ___

### Stage 5: GitLab API Project

- [ ] **Done with Stage 5**

1. [ ] Develop a small GitLab API integration / automation using two or more
APIs of your choice. Put it on your GitLab.com account and link to it here.
   - Link: ___

### Penultimate Stage: Review
You feel that you can now do all of the objectives:

- Familiar with API
- Know where to look in the code
- Make use of the API
- Can troubleshoot API questions

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself!

- [ ] Update ...

### Final Stage

1. [ ] Have your trainer and/or manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Send a MR to declare yourself a **GitLab API Expert** on the team page.

/label ~module
