---
module-name: "Support Manager Basics"
area: "Customer Service"
maintainers:
  - vijirao
---

## Introduction

Welcome to the Support Manager focused onboarding issue!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and public](https://about.gitlab.com/handbook/values/#transparency).**

**Goals of this checklist**

At the end of the checklist, you will be
- able to understand the existing support manager workflows and processes
- aware of the company resources available to effectively manage your team, and to motivate, coach, mentor and guide your team
- able to access and understand the support metrics and dashboards
- able to understand our interview process and tools

**General Timeline and Expectations** 

- This issue should take you **two weeks to complete**.
- Note that, as a manager, you will be pulled into day to day requirements and activities of your team from as early as week 3 or as soon as you have your team transtitioned to you. 

## Stage 0: Support Manager Workflows & Processes

1. [ ] If not done already, have your on-boarding buddy invite you to #spt-hiring-mgmt and #spt_managers-internal.
1. [ ] Learn how Support Management at GitLab [keeps track of the issues and projects](https://about.gitlab.com/handbook/support/managers/#how-to-see-what-the-support-managers-are-working-on) that we are working on.
1. [ ] To ensure we are in constant communication with our counterparts in other regions, Support Management runs cross-regional sync meetings weekly. Read about the [purpose and recommended procedure to run these](https://about.gitlab.com/handbook/support/managers/#support-leadership-meetings).
1. [ ] Read about the [Support Engineer Responsibilities](https://about.gitlab.com/handbook/support/support-engineer-responsibilities.html) to understand what the focus areas will be for your team and how you can best support them to excel in those areas.
1. [ ] Read about [the tools](https://about.gitlab.com/handbook/support/managers/#metrics) we use in Support Management to generate metrics.
   1. [ ] Login to [Zendesk Explore](https://gitlab.zendesk.com/explore/) and review the Support Metrics dashboard.
   1. [ ] Engage in a pairing session with a support manager (outside of your region if possible) to learn about what reports they use regularly and why it is useful to them.
1. [ ] Understand your responsibilities for reviewing and merging troubleshooting documentation
   1. [ ] Read up on the [historical context](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1765#background) behind why Support Managers have maintainer access to production repos.
   1. [ ] **With Great Power Comes Great Responsibility:** Maintainer access gives you the ability to merge any MR in a repository. Please understand that you should [only merge MRs that make changes to the troubleshooting sections](https://docs.gitlab.com/ee/development/documentation/styleguide.html#all-information) in the documentation; otherwise general doc MRs should be assigned to the [relevant technical writer](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers) and code MRs should follow the [code review process](https://docs.gitlab.com/ee/development/code_review.html).
   1. [ ] Read the [Troubleshooting section](https://docs.gitlab.com/ee/development/documentation/styleguide.html#troubleshooting) of the GitLab Documentation Style Guide
   1. [ ] Create an [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to request `maintainer` access to the following repos: [gitlab](https://gitlab.com/gitlab-org/gitlab), [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab), [gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner), [charts/gitlab](https://gitlab.com/gitlab-org/charts/gitlab). 
       * Use the following justification: `[Support managers should have merge access to repos housing docs](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1769)`
1. [ ] Learn about the Manager On-Call rotations and expectations.
   1. [ ] Read about the [Support Manager On-Call](https://about.gitlab.com/handbook/support/on-call/#manager-on-call) process.
   1. [ ] Learn about the [SSAT Feedback Issues](https://gitlab.com/gitlab-com/support/feedback/-/tree/master) and the [SSAT Reviewing Manager](https://about.gitlab.com/handbook/support/workflows/how-to-respond-to-feedback.html) responsibilities.
   1. [ ] Have a discussion with your manager on when to add yourself to these rotations on PagerDuty. Typically, you should be ready to be added to the rotation by the beginning of your 5th week here.
1. [ ] Arrange to join the Manager On-Call rotations.
   1. [ ] Support Manager On-Call
      1. 1. [ ] Create a new Issue in the `[support-team-meta](https://gitlab.com/gitlab-com/support/support-team-meta)` Project requesting you be added to the `Support Manager On-Call` PagerDuty rotation for your region. 
      1. [ ] Assign the Issue to your Manager for approval and implementation.
   1. [ ] SSAT Reviewing Manager
      1. [ ] Create a new Issue in the `[support-team-meta](https://gitlab.com/gitlab-com/support/support-team-meta)` Project requesting you be added to the `SSAT Reviewing Manager` PagerDuty rotation for your region. 
      1. [ ] Assign the Issue to your Manager for approval and implementation.
      1. [ ] Locate your PagerDuty ID by navigating to your profile and noting the 7-character unique string at the end of the URL: `https://gitlab.pagerduty.com/users/AAAAAAA`
      1. [ ] Add your PagerDuty ID to the following file, following instructions [here](https://gitlab.com/gitlab-com/support/feedback/-/tree/master#how-it-works): `https://gitlab.com/gitlab-com/support/feedback/-/blob/master/managers.yml`


### Stage 1: Get to know your team members!

Try to get through this section **before** you have your team transitioned to you. If that happened before you got to this stage, don't worry! You can always come back, refer and iterate.

1. [ ] Review the [Getting to Know Your Team Members](https://about.gitlab.com/handbook/support/managers/getting-to-know-you.html) handbook page. This page will help you get prepared for the transition of your new team members. Use it as a guideline as you work with the manager who is transitioning team members over to you.
1. [ ] 1:1s are an important part of fostering the relationship between you and your team member. Review the [Support 1:1s page](https://about.gitlab.com/handbook/support/managers/support-1-1s.html) for useful pointers and recommendations on how you can make 1:1s more efficient for you and your team member. Remember, these are only guidelines and you should tailor them based on individual requirements and expectations.

## Stage 2: Hiring Tools & Processes

Talk to your onboarding buddy to understand what hiring is happening right now. They will let you know about timelines and give you an overview of when you can start participating in interviews.

1. [ ] Complete the [support interview training issue](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Module%20-%20Support%20Interview%20Training) to learn about how assessments and technical interviews are conducted. Reach out to People Ops if it was not created for you.

## Stage 3: Support Focuses and Learning Pathways

Support Engineers have different sets of tasks to perform to help our customers depending on their primary focus. As a manager, it is important to understand this as it will help you guide your team on prioritization and load balancing. Though you will learn about this through different channels and mediums as you progress through onboarding, take a look at the [area of focus onboarding pathway content for support engineers](https://about.gitlab.com/handbook/support/training/#support-engineer-area-of-focus-pathway) to get a quick idea of the work involved in the different focuses:

(Note: It is not mandatory for you to complete these modules - however, feel free to do so if you are interested!)

1. [ ] Review the content under the [Self Managed Support Learning Pathway](https://about.gitlab.com/handbook/support/training/#self-managed-support-learning-pathway).
1. [ ] Review the content under the [GitLab.com SaaS Support Learning Pathway](https://about.gitlab.com/handbook/support/training/#gitlabcom-saas-support-learning-pathway).
   1. [ ] At this stage, if you will be having Support Engineers focusing on SaaS reporting to you, get yourself added to the [Dotcom Support Sync Chair/Note-taker rotation](https://docs.google.com/document/d/1ALqRSCvQ9a0Ss7HeT5JuOUOZ7H1hBOITC03BYcAIkaE/edit#heading=h.i5apkwajdxlc) against the regional call that you will be able to attend. You can find this meeting in the GitLab Support calendar.
1. [ ] Review the content under the [License and Renewals Learning Pathway](https://about.gitlab.com/handbook/support/training/#license-and-renewals-learning-pathway).

## Stage 4: Pair with Support Engineers!

- Perform at least **5 pairing sessions** with Support Engineers - pair with at least 2 Dotcom and 2 Selfmanaged focus engineers. These sessions will help you understand the different workflows in action, see how we manage tickets and customers day to day, and might also give you ideas for process improvements! You can complete this stage as you work through the other onboarding modules in your pathway.
- Consider people outside of your immediate region to avoid silos.

1. [ ] Familiarize yourself with the [Support Pairing Project](https://gitlab.com/gitlab-com/support/support-pairing) and [summary of pairings page](https://gitlab-com.gitlab.io/support/support-pairing/).
1. [ ] Create a Google Calendar event, inviting that person. If you're unsure of times due to timezones, feel free to send them a message in Slack first. When it's time for the pairing session, create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing/-/issues) and use the appropriate template for the call. 
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
1. [ ] Shadow at least 2 emergencies with Support Engineers. This will help in understanding the workflow, and also give a first hand experience on how GitLab handles emergencies. You can do this by one of the following methods:
- Look out for Emergency PagerDuty notifications in the #support_self-managed slack channel, or,
- Request SEs in your region to tag you when they get paged for an emergency, or,
- Consider joining a shadow rotation and getting paged right along with the [Support Engineer On-Call](https://about.gitlab.com/handbook/support/on-call/#how-support-on-call-works) - your onboarding buddy can help you with this
Add the emergency ticket links below:
   1. [ ] `Ticket Link`
   1. [ ] `Ticket Link`

## Congratulations! You made it!

You are now ready to continue on your onboarding path to tackle the next module in line. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [training page](https://about.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under your Onboarding pathway!

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
