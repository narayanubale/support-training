---
module-name: "Zendesk Basics"
area: "Customer Service"
maintainers:
  - atanayno
---

## Introduction

Zendesk is our Support Center and the main communication line with our customers. This module contains some knowledge required to work with Zendesk efficiently as a GitLab Support Engineer.

**Goals of this checklist**

At the end of the checklist you will be able to:
- utilize Zendesk to perform ticket management tasks
- access Salesforce and the Customer portal to look up customer & account information

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **1 day to complete**.


### Stage 1: Prerequisites

- [ ] **Done with Stage 1**
<details>
<summary>Tasks</summary>

   Make sure that you have reviewed the Zendesk resources listed in the onboarding issue under the section **Initial Zendesk training**:

  1. [ ] Complete Zendesk Agent training
     1. [ ] Sign up at [Zendesk university](https://training.zendesk.com/auth/login?next=%2F)
     You'll receive an email with information on accessing the Zendesk courses
     1. [ ] Complete the [Zendesk Overview: Support](https://training.zendesk.com/zendesk-overview-support/) course (approx. 10 min)
  1. [ ] Review additional Zendesk resources
     1. [ ] [UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
     1. [ ] [Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
     1. [ ] [Working w/ Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets)
        *(Read [Avoiding Agent Collisions](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets#topic_ryy_42g_vt) carefully)*
     1. [ ] [Using Macros](https://support.zendesk.com/hc/en-us/articles/203690796-Using-macros-to-update-tickets)
     1. [ ] [Triggers and how they work](https://support.zendesk.com/hc/en-us/articles/203662246)
     1. [ ] [Macros Creation and Editing at GitLab](https://about.gitlab.com/handbook/support/workflows/macros.html)
     1. [ ] [Language Translation using Unbabel](https://about.gitlab.com/handbook/support/workflows/unbabel_translation_in_zendesk.html)
     1. [ ] [Formatting text with Markdown](https://support.zendesk.com/hc/en-us/articles/203691016-Formatting-text-with-Markdown)
     1. [ ] [Marking tickets as spam](https://about.gitlab.com/handbook/support/workflows/marking_tickets_as_spam.html); more details on the [alternate way](https://support.zendesk.com/hc/en-us/articles/203691106-Marking-a-ticket-as-spam-and-suspending-the-requester) to mark tickets as spam.
     1. [ ] Bookmark the [Zendesk Support search reference](https://support.zendesk.com/hc/)
  1. [ ] Check your personal Zendesk signature.
     1. [ ]  In Zendesk, click your user icon in the upper-right corner and select View Profile. Under Signature in the left sidebar, check the field. 
     An example agent signature:
       ```
       {{agent.name}}
       GitLab Support
       ```
     1. [ ] [Agent signatures](https://gitlab.com/gitlab-com/support/support-ops/agent-signatures) are  managed by Service Operations. Please ask in [#service_operations](https://gitlab.slack.com/archives/C018ZGZAMPD) if you do not see your signature in Zendesk. 
     1. [ ] Your personal Zendesk signature is shown at the end of every ticket response. You can update this to include a personalized valediction such as `Thanks` or `Best Regards` by editing the [support-team.yml](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) file, but it's better to end each message with words that match the situation of that ticket. 

</details>

### Stage 2: Studying ticket lifecycle

- [ ] **Done with Stage 2**
<details>
<summary>Tasks</summary>

  **Note:** we have created a test organization, `Zendesk Basics Module`, for this Module. Please use it for all the tasks below.

  This section describes the common lifecycle of a ticket and explains how to use some Zendesk features while working with tickets.
  Each theoretical part is accompanied by some practical steps. Be sure to go through all the practical steps to make sure that you understand Zendesk behavior
  correctly in each case.

  1. [ ] **Ticket statuses:** read the article about [ticket statuses](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#understanding-ticket-status)
  to understand how we use them in GitLab Support.

  1. [ ] **Creating new users:** sometimes you may need to create a new user on behalf of a customer. Popular use case: a customer asks to add somebody to CC but
  this user does not exist in Zendesk. To do it:
     - Point your cursor to the `+Add` button and pick `User` in the dropdown.
     - Specify the email and the name and click `Add`.
     <br>

     **Practice:**
     - [ ] Create a Zendesk user associated with one of your personal email addresses.
     <br>
  1. [ ] **Creating a new ticket:**

      ##### 1. How customers can create tickets

      GitLab customers can submit tickets by navigating to our [Support Portal](https://support.gitlab.com/hc/en-us/requests/new) and completing the support form.

      ##### 2. How support agents can create tickets

      Sometimes you may need to create a new ticket for a customer. Common use cases include:
       - Creating a follow-up ticket for an emergency
       - A ticket is too long and it contains a mix of various issues, so you want to move one of those issues to a new ticket
      <br>

     To create a ticket as a support agent:
       - Point your cursor at the `+ Add` button and select `Ticket` in the dropdown
       - Fill the `Requester` field and the CC list
       - Pick the appropriate form
       - Add a subject and a description
       <br>

      **Notes:**
        - After a support agent creates and submits a ticket as `New` or `Open`, the ticket will have no SLA. SLA is assigned only when a customer updates a ticket.
        It is recommended to send a reply to the customer and submit a ticket as `Pending`, `Solved` or `On-hold`, depending on the situation.
        - When a ticket is submitted by an unknown user, a user account will be automatically created by Zendesk.
        <br>

      **Practice:**
      - [ ] Create a ticket as a customer by completing the form from the [Support Portal](https://support.gitlab.com/hc/en-us/requests/new).
           - Choose `Support for GitLab.com (SaaS)` for the reason.
           - Use the same personal email address you used when you created the new user from above.
           - Use the subject "***IGNORE*** Test ticket only for ZD Basics Onboarding ***IGNORE***".
           - Add in a description (making sure to reiterate that the ticket is ONLY a test and can be ignored).
           - Choose `Free user` for the GitLab subscription.
           - Choose any Problem type.
           - Provide a test Username.
           - Use `test/test` for the GitLab.com Project Path.
           - Set the Priority to `low`.
           - Choose your preferred region (your choice).
           - Submit the form and look for an automated reply to your personal email.
      - [ ] Logged in as an agent, find the new ticket in the `Free/Self-Provisioned Trial Support` Zendesk view and take ownership.
      - [ ] Note this [current Zendesk limitation](https://support.zendesk.com/hc/en-us/community/posts/360038476034-Markdown-for-customers) (markdown only works for support agents, not for customers).

      <br>
  1. [ ] **Associating a user with an organization:**
     - Click the username and find the field `Org.` in the list of fields.
     - Paste or start typing the name of the organization. Note that if you copy the organization name from somewhere else, it is important to remove leading
     and trailing spaces.
     - If the organization exists in Zendesk, it will be shown in the dropdown. After clicking it, you will see the message `<username> was successfully updated`,
     which means that the user was added to the organization.

     <br>

     **Note:** There are several use cases related to this process. For more information see the article
     [Associating needs-org tickets with appropriate organization](https://about.gitlab.com/handbook/support/workflows/associating_needs_org_tickets_with_orgs.html).
     One of the most common cases is that there is a new user in the customer’s organization
     and the customer asks us do add this user to their organization record in Zendesk.
       <br>

     - [ ] Learn about [how organizations are created and updated in GitLab's Zendesk](https://about.gitlab.com/handbook/support/workflows/zendesk_organizations_and_users_overview.html).

      **Practice:**
      - [ ] Add the previously created user to the organization `Zendesk Basics Module`. Note that
      your ticket now has the 24h SLA assigned to it, since the organization has the `Starter` support level assigned.
      <br>
  1. [ ] **Modifying CC list in tickets**. To add yourself, a customer or another GitLab
     team member to the CC list in the ticket:
     - Open the ticket in the agent interface.
     - Copy the email address you need to add and paste it to the `CCs` field.
     - Click `Submit` to save your change.
     - Note that clicking submit will also save any text that you have entered in the "Public reply" or
       "Internal note" field. This happens whether the ticket is submitted in the same state or in a new state.
     - Zendesk will send only public replies to non-GitLab users who are included in the CC list of a ticket,
       whereas it will send all replies, public or internal, to the Support Agents in the CC list.
     <br>

     **Note:** At the moment the ability to add addresses to the CC list via the support portal
     is disabled due to [this security issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1278),
     which is why we need to perform this action for customers.

     If you need to remove someone from the CC list:
        - Open the ticket in the agent interface.
        - Click the cross near the specified email address to remove it and click `Submit` to save the change.
      <br>

      **Practice:**
      - [ ] Add your work email address to the CC list of the previously-created ticket.
      <br>
  1. [ ] **Changing a ticket's form and sending replies:**

     In most cases the customer selects the correct ticket form when they submit a ticket.
     The two main causes of a ticket having the wrong form are:
        - The customer made a mistake. Given this possibility, please always review the
          contents of a new ticket and change the form if it is incorrect.
        - The customer submitted the ticket via email, causing Zendesk to assign the form
          `Other`. Please change the form to the correct value for such tickets.
        Setting the correct form will route the ticket to the correct view in Zendesk. Note that
        are some times when the `Other` form is the correct one, such as when the request
        is not about our product, but instead is about topics such as hiring status,
        invitations to sponsorship, conferences, etc.
      <br>

      **Practice:**
      - [ ] Change the ticket's form from `Other` to `Self-managed` or `GitLab.com`, depending on your role.
      - [ ] Change `Self-managed Problem Type` to `Other`.  As this is a test case we can leave it as 'Other',
        however when solving real tickets please choose the correct problem type.
      - [ ] As a support agent, send a reply to the ticket and set the ticket to **Pending**.
      - [ ] You will get a message in your personal email. Now reply back from your personal email
        and check Zendesk; you will see that the ticket is back,
      it has **Open** status and SLA is shown in green.
      <br>
  1. [ ] **Using macros in tickets**

     A macro is a message template that you can use as a starting point for a public
     response in a common situation, such as:
        - You need to provide a CE user with information about free support resources.
        - You need to explain to a GitLab.com user what they can do to reset 2FA.
        - After the call with a customer, you would like to summarize everything that was done during the call.

      Please edit the message contents after applying the macro so that the message
      is delivered in your own voice and is appropriate for the ticket.

      To add a macro to your reply:
       - Open a ticket.
       - Click the `Apply Macro` button.
       - Search for the macro if you know its name or find it in the list, and then
         click it. The macro will be added to the comment field of your ticket.
      <br>

      **Practice:**

      - [ ] Let's pretend that you scheduled a call with customer. In order to meet the SLA,
        reply to the ticket and set it to **On-hold**.
        Note that this action reset the SLA clock, and assigned the ticket to you.
        If you leave it in the **On-hold** status for 4 days, it will be reopened but there will be no SLA.
      - [ ] Suppose that you held a call with a customer and fixed the problem they had reported.
        Add the **Post customer call** macro to the ticket, fill in the template provided
        by the macro and set the ticket's status to **Solved** when sending the update.
        Check that you got email messages in your personal email inbox regarding all these actions.
      <br>

 1. [ ] **Zendesk Triggers:** read about [Zendesk triggers](https://about.gitlab.com/handbook/support/support-ops/zendesk/#triggers) to understand how we use them in GitLab Support.

 1. [ ] **Changing Priority and using a macro** This exercise combines a couple
     of the previous exercises. Start by reading about [ticket priority](https://about.gitlab.com/handbook/support/workflows/setting_ticket_priority.html).

      **Practice:**

      - [ ] Change the priority on your ticket and use the appropriate macro as part of your response.
      <br>

 1. [ ] **Merging tickets:** Sometimes customers create duplicate tickets. In order to avoid
  confusion it is a good idea to merge them into one.
  Let's imagine we have tickets `0001` and `0002`, and we want to merge `0002` into `0001`. To do it:
      - Open the ticket `0002`.
      - Click the arrow to the far right of the ticket subject, and select `Merge into another ticket`.
      - The `Ticket merge` dialogue will appear. Enter the ticket ID `0001` into the field `Enter ticket ID to merge into` and click `Merge`.
      - In the next window it is important to pay attention to the `Requester can see this comment` checkboxes.
      It is recommended to check the checkbox for the ticket `0002` so that the customer will see that the ticket `0002` was merged into `0001`.
      For the ticket `0001`, it is better to uncheck this checkbox, so that the ticket doesn not lose its SLA and move to the end of the queue,
      potentially causing delays in replies.
      - **Important:** note that it is [not possible to unmerge tickets](https://support.zendesk.com/hc/en-us/articles/115004245847-Can-I-un-merge-tickets-). Be careful and thoroughly check everything before merging tickets.
      <br>

      **Practice:**
      - [ ] Create two tickets using the [Support Portal](https://support.gitlab.com/hc/en-us/requests/new). After that, as an agent, merge the second ticket into the first one.
      <br>

  1. [ ] **Cleanup:** after you are done with all the tests, please close all your tickets:
      - You may mark them as **Solved**. Such tickets will move to **Closed** automatically after 4 days according to
      [Understanding Ticket Status](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#understanding-ticket-status).
      - You may mark them as **Pending** but note that if you do so for a ticket that has SLA, you should send a public reply when moving it to **Pending**.
      After 7 days in the **Pending** status, the ticket will be marked as **Solved**.
      - Make sure that there are no test tickets left in **On-hold** status as they will move to **Open** in 4 days.

</details>

### Stage 3: Zendesk Instances

If you are a U.S. Citizen, you will be working in the U.S. Federal instance in addition
to the standard instance. All other team members should be aware that there is a U.S. Federal
instance and who it's for.

- [ ] Read about [the different Zendesk instances](https://about.gitlab.com/handbook/support/workflows/zendesk-instances.html).

### Stage 4: Looking up Customer & Account Information
- [ ] **Done with Stage 4**
<details>
<summary>Click to expand/contract</summary>


We use several tools to look up customer & account information.

**Salesforce**

1. [ ] Familiarize yourself with the workflow for [looking up customer information](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html).
1. [ ] Watch the *How to use Salesforce from a support perspective* video linked on the workflow page above.
1. [ ] Familiarize yourself with how to [log in to Salesforce](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-salesforce).

**Customer Portal and LicenseDot App**

1. [ ] Read about and be aware of the [Customer Portal admin page](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-customersgitlabcom), and the [LicenseDot App] (https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-licensegitlabcom). 

If you will primarily focus on License & Renewals, you should be given access to the above tools, along with training on the [License & Renewals workflows](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/).

</details>

#### Congratulations! You made it, and now have a basic understanding of using Zendesk for ticket management!

You are now ready to continue on your onboarding path to tackle the next module in line based on your role. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [onboarding page](https://about.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under the Onboarding pathway.

Please also submit MRs for any improvements to this training that you can think of. The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
