---
module-name: "GitLab Runner"
area: "Product Knowledge"
gitlab-group: "Verify:Runner"
maintainers:
  - TBD
---

**Title:** _"GitLab Runner - your-name"_

Recommended is to tackle the stages as defined below, you don't have to complete all of the steps in a stage before you can move on.

**Goal of this checklist:** Set a clear path for GitLab Runner training

### Stage 1: Commit and Become familiar with what GitLab Runners are

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GitLab Runner questions to you
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Start with reading about [GitLab Runners](https://docs.gitlab.com/runner/) (No need to follow any links on the page)
1. [ ] [Install a GitLab Runner on preferred OS](https://docs.gitlab.com/runner/install/linux-repository.html)
1. [ ] Create an example project on your own GitLab instance.

### Stage 2: Basic GitLab Runner

- [ ] **Done with Stage 2**

1. [ ] Learn about possible variables in the [.gitlab-ci.yml docs](https://docs.gitlab.com/ee/ci/yaml/README.html)
1. [ ] Read up on [Executors](https://docs.gitlab.com/runner/executors/)
1. [ ] Register your runner as a [Shared Runner](https://docs.gitlab.com/ee/ci/runners/README.html#registering-a-shared-runner) on your instance.
1. [ ] Register your runner as a [Specific Runner](https://docs.gitlab.com/ee/ci/runners/README.html#registering-a-specific-runner) on your project, and test that it now runs the builds for your project.
1. [ ] Read about [Pipelines and Jobs](https://docs.gitlab.com/ee/ci/pipelines.html).
1. [ ] Learn about [Using tags](https://docs.gitlab.com/ee/ci/runners/README.html#use-tags-to-limit-the-number-of-jobs-using-the-runner)

### Stage 3: Intermediate GitLab Runner

- [ ] **Done with Stage 3**

1. [ ] [Install a GitLab Runner through Docker image](https://docs.gitlab.com/runner/install/docker.html)
1. [ ] Read about [What is a Service](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service)
1. [ ] Configure a Service to be used in a pipeline
1. [ ] Read the [troubleshoot section for GitLab Runner](https://docs.gitlab.com/runner/faq/)

### Stage 4: Advanced GitLab Runner

- [ ] **Done with Stage 4**

1. [ ] Read [Install and register GitLab Runner for autoscaling with Docker Machine](https://docs.gitlab.com/runner/executors/docker_machine.html)
1. [ ] Read how to [install GitLab Runner on Windows](https://docs.gitlab.com/runner/install/windows.html)
1. [ ] Configure a [self-signed certificate](https://docs.gitlab.com/runner/configuration/tls-self-signed.html#self-signed-certificates-or-custom-certification-authorities) for a runner.
1. [ ] Install [GitLab Runner Kubernetes clusters](https://docs.gitlab.com/runner/executors/kubernetes.html)

### Optional: Pipeline section

- [ ] **Done with the Optional Pipeline section**

1. [ ] [Multi-project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html)
1. [ ] [Parent-Child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)
1. [ ] [Pipelines API](https://docs.gitlab.com/ee/api/pipelines.html)
1. [ ] Speed up a pipeline using [caching](https://docs.gitlab.com/ee/ci/yaml/#cache).
1. [ ]

### Stage 5: Tickets

- [ ] **Done with Stage 5**

1. [ ] Answer 7 GitLab Runner tickets and paste the links here, even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay it to the customer.

   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __

### Final Stage

1. [ ] Have your trainer and manager review this issue.
1. [ ] Manager: Have a GitLab Runner expert in Support review 3-5 of the tickets from Stage 5 and report back to you on whether sufficient GitLab Runner knowledge has been demonstrated.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to declare yourself a **Runner Expert** on the team page.
1. [ ] Add this module to the list of training you have completed!

/label ~module
