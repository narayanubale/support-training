---
module-name: "Gitlab Installation and Administration Basics"
area: "Product Knowledge"
gitlab-group: "Enablement:Distribution"
maintainers:
  - TBD
---

## Introduction

Welcome to the next step in your onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

## Goals of this checklist

At the end of the checklist, new team member should
- Get your development machine set up.
- Familiarize yourself with the codebase.
- Be prepared to reproduce issues that our users encounter.
- Be comfortable with the different installation options of GitLab.
- Have an installation available for reproducing customer bug reports.

## General Timeline and Expectations*

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **one week to complete**.

### Stage 0. Installation and Administration basics.

**Set up your development machine**

1. [ ] Install the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/). If you like Docker, especially if you're running Linux on your desktop, consider the [GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit/).

**Installations**

You will keep one installation continually updated on Google Cloud Platform (managed through Terraform), just like many of our clients. You need to choose where you would like to test
other installations. _(TODO: We need to list some benefits of each choice here.)_

1. [ ] Use terraform to create a new test instance.
    1. [ ] Read up on [how to create a new instance](https://gitlab.com/gitlab-com/support/support-resources/-/blob/master/README.md). If you have any issues or questions, ask in the `#support_self-managed` channel.
1. [ ] Set up your [test environments](https://about.gitlab.com/handbook/support/workflows/test_env.html).
1. [ ] Choose between Local VMs or GCP for your preferred test environment and note it in a comment below.
1. [ ] Perform each of the following [Installation Methods](https://about.gitlab.com/installation/)
   on the test environment you chose above:
   1. [ ] Install via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab)
   1. [ ] Populate with some test data: User account, Project, Issue
        - [Utilize the GitLab API to seed your test instance](https://gitlab.com/gitlab-com/support/support-training/blob/master/seed-data-api.md)
   1. [ ] Backup using our [Backup rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
   1. [ ] Install via [Docker](https://docs.gitlab.com/ee/install/docker.html) on your local machine.
   1. [ ] Restore a backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-a-previously-created-backup)
   1. [ ] Install GitLab from [Source](https://docs.gitlab.com/ee/install/installation.html).
      Installation from source is not common but will give you a greater understanding
      of the components that we employ and how everything fits together.
        - [Have a copy of GitLab source code available offline for reference](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1900)
1. [ ] With your onboarding buddy or another Support team member, manually generate 3 ' self-managed' licenses at https://license.gitlab.com/ - one for each of Starter, Premium, and Ultimate. Suggest around 100 users with 5-year expiry. Keep the licenses securely in 1Password. Use the licenses on your test instance when replicating customer issues. By having the correct license, you will ensure feature parity with what the customer is seeing.
1. The services below are part of the many services used by our customers. Installing these services and learning how they work with GitLab will prepare you for Stage 3 and give you a solid foundation for when you're ready to resolve tickets on your own.
    1. [ ] [GitLab Runner](https://docs.gitlab.com/runner/) and connect it to your GitLab instance.
    1. [ ] [Elastic Search](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html)
        1. [ ] Install on a VM or a separate instance on Terraform
            * The [steps outlined in our support resources documentation](https://gitlab.com/gitlab-com/support/support-resources/-/blob/master/README.md) should help you configure your instance's IP.
            * Set `cluster.name` and `node.name` and use the latter in `cluster.initial_master_nodes` to enable ["single node" mode](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/add-elasticsearch-nodes.html).
        1. [ ] [Connect Elastic Search to your GitLab instance and index your repositories](https://docs.gitlab.com/ee/integration/elasticsearch.html). You need to apply a license to your GitLab instance for the integration settings to appear if you haven't already.
    1. [ ] Connect your GitLab instance to a cluster via the [Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/) integration and install a Runner in the cluster:
        1. [ ] log in to https://console.cloud.google.com/ with your work email.
        1. [ ] Create a cluster under the [project `gitlab-support`](https://console.cloud.google.com/kubernetes/list?project=support-testing-168620).
           Start small to [cut costs to about one tenth](https://about.gitlab.com/handbook/values/#spend-company-money-like-its-your-own) of the default cluster config:
              - Set `Location type` to a single zone (instead of a region)
              - Set `Node pool` `Size` to 1
              - Set the `Nodes`' `Machine type` to a `smaller` one than the default (recommend `e2-medium` for 2vCPU and 4GB RAM)
              - Review the [general tips for cost optimization](https://about.gitlab.com/handbook/support/workflows/cost-optimizations-with-cloud-instances.html#general-tips-for-cost-optimization)
        1. [ ] Find your cluster and click `connect` next to your cluster. Then click on `Run in Cloud Shell`.
        1. [ ] [Add](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) the Kubernetes cluster to GitLab. Use the [Cloud Shell](https://cloud.google.com/shell/docs/how-cloud-shell-works) to run the `kubectl` commands as needed.
            - Be aware that when copying data from the Cloud Shell, it might add extra characters to soft wrapped strings. 
        1. [ ] Create a project on your self-managed instance and then create a file named `.gitlab-ci.yml`.
        1. [ ] Use [the YAML docs](https://docs.gitlab.com/ee/ci/yaml/#parameter-details) as a resource to build a basic script.
        1. [ ] Make sure your project has access to your Kubernetes cluster under Operations -> Kubernetes.
        1. [ ] Make sure your project has registered runners under your project's Settings -> CI/CD -> Runners.
        1. [ ] Pat yourself on the back; you're now ready to reproduce some of our common customer tickets.
        1. [ ] Remove the cluster when you're done!
1. [ ] Watch the [GitLab Debugging Techniques: A Support Engineering Perspective](https://www.youtube.com/watch?v=9W6QnpYewik) video on GitLab Unfiltered (YouTube). This dives into some common issues that customers might experience.
1. [ ] In the future, you may need other applications for testing. Don't set these up now, but check out the [infrastructure for troubleshooting section](https://about.gitlab.com/handbook/support/workflows/#Infrastructure%20for%20troubleshooting) of workflows, so you know what's available.
1. [ ] You can check this box, but this one never stops as long as you are a Support Engineer for GitLab:
       Keep this installation up-to-date as patch and version releases become available, just like our customers would. If your test instance is publicly available, keep it secured and disable user Sign-ups.

#### Congratulations! You made it, and now have an understanding of the different ways in which GitLab can be installed and managed.

You are now ready to continue on your onboarding path to tackle the next module in line. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [training page](https://about.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under your Onboarding pathway!

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
