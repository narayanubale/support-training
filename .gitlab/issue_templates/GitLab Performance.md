---
module-name: "GitLab Performance"
area: "Troubleshooting & Diagnostics"
maintainers:
  - TBD
---

**Title:** _"GitLab Performance Module - **your-name**"_

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

### Stage 1: Commit to learning about GitLab Performance

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started.
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Notify the team to start routing performance related customer tickets to you.

### Stage 2: GitLab's Architecture & Internals

- [ ] **Done with Stage 2**

This stage is aimed for you to have a good grasp of GitLab internals.

- [ ] Review GitLab's Architecture, Components, Request Type & System Layout
    1. [ ] GitLab's [Architecture Overview](https://docs.gitlab.com/ee/development/architecture.html)
    1. [ ] GitLab.com's [Production Architecture overview](https://about.gitlab.com/handbook/engineering/infrastructure/production-architecture/)
    1. [ ] GitLab.com's [cloud native high level architecture](https://docs.google.com/drawings/d/1mAC0qEEVHzHqy4kbcnDka0TVikzttDSJBrq1hQzQ5-Q/edit)

### Stage 3: GitLab's HA Installation methods

- [ ] **Done with Stage 3**

This stage is aimed at you having a high level understanding of the different High-Availability Installation methods.

1. [ ] [Geo](https://docs.gitlab.com/ee/administration/geo/replication/index.html)
1. [ ] [Scaling and High Availability](https://docs.gitlab.com/ee/administration/high_availability/)
1. [ ] [GitLab HA on AWS](https://docs.gitlab.com/ee/install/aws/index.html)
1. [ ] [Cloud Native charts](https://docs.gitlab.com/charts/)


### Stage 4: GitLab/Linux Performance Analysis & Troubleshooting

- [ ] **Done with Stage 4**

This stage lists resources to tools/performance topics that are usually experienced by GitLab's customers

- [ ] Familiarity GtiLab Support Toolbox
    - [ ] [GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos)
    - [ ] [GitLabSOS Analyzer](https://gitlab.com/gitlab-com/support/toolbox/sos-analyzer)
    - [ ] [strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser)
- [ ] GitLab Tools
    - [ ] [GitLab Performance Bar](https://docs.gitlab.com/ee/administration/monitoring/performance/performance_bar.html)
- [ ] Git Performance:
    - [ ] [Troubleshooting Git](https://docs.gitlab.com/ee/topics/git/troubleshooting_git.html)
    - [ ] [git-fsck](https://git-scm.com/docs/git-fsck)
    - [ ] [git-sc](https://git-scm.com/docs/git-gc)
    - [ ] [git prune](https://www.atlassian.com/git/tutorials/git-prune)
- [ ] NFS
    - [ ] [GitLab HA with NFS](https://docs.gitlab.com/ee/administration/high_availability/nfs.html)
    - [ ] Stan Hu's [How we spent two weeks hunting an NFS bug in the Linux kernel](https://about.gitlab.com/blog/2018/11/14/how-we-spent-two-weeks-hunting-an-nfs-bug/)
    - [ ] [What we're doing to fix Gitaly NFS performance regressions](https://about.gitlab.com/blog/2019/07/08/git-performance-on-nfs/)
    - [ ] [Identifying NFS performance bottlenecks](https://docstore.mik.ua/orelly/networking_2ndEd/nfs/ch16_04.htm)
    - [ ] [Optimizing NFS Performance](http://www.iitk.ac.in/LDP/HOWTO/NFS-HOWTO/performance.html)
    - [ ] [How to do Linux NFS Performance Tuning and Optimization](https://www.slashroot.in/how-do-linux-nfs-performance-tuning-and-optimization)
- [ ] PostgreSQL Performance
    - [ ] GitLab's [Database settings](https://docs.gitlab.com/omnibus/settings/database.html)
    - [ ] [Configure GitLab using an external PostgreSQL service](https://docs.gitlab.com/ee/administration/external_database.html)
    - [ ] [Configuring PostgreSQL for Scaling/High Availability](https://docs.gitlab.com/ee/administration/high_availability/database.html)
    - [ ] [Understanding Postgres Performance](http://www.craigkerstiens.com/2012/10/01/understanding-postgres-performance/)
- [ ] AWS Rate Limits
    - [ ] [EFS Rate Limits](https://docs.aws.amazon.com/efs/latest/ug//limits.html)
    - [ ] [Other AWS Rate Limits](https://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html)
- [ ] Linux System Performance Tracing/Analysis
    - [ ] [Linux Performance Analysis in 60,000 Milliseconds](https://medium.com/netflix-techblog/linux-performance-analysis-in-60-000-milliseconds-accc10403c55)
    - [ ] [BPF(Berkeley Packet Filters) Compiler Collection](https://github.com/iovisor/bcc)
    - [ ] [Linux Performance and Tuning Guidelines](https://lenovopress.com/redp4285.pdf) [PDF]
    - [ ] [Linux kernel profiling with perf](https://perf.wiki.kernel.org/index.php/Tutorial)
    - [ ] [perf Examples](http://www.brendangregg.com/perf.html)
    - [ ] Brendan Gregg's [Linux Performance](http://www.brendangregg.com/linuxperf.html)

### Stage 5: Tickets

- [ ] **Done with Stage 5**

- [ ] Contribute valuable responses on at least 10 performance related tickets, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 6 (WIP): Customer Calls

- [ ] **Done with Stage 6**

- [ ] List atleast 5 tickets that was taken to a call and you were able to resolve or push forward with the knowledge acquired so far.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Final Stage

- [ ] Your manager needs to check this box to acknowledge that you have finished.
- [ ] Send a MR to declare yourself an **GitLab Performance Expert** on the team page.

/label ~module
