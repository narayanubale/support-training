---
module-name: "GitLab-com Console"
area: "Product Knowledge"
maintainers:
  - TBD
---


### Overview

Your manager's approval _must_ be obtained before starting this module. Basic Ruby, Ruby on Rails and [Ruby Interactive Shell](https://www.digitalocean.com/community/tutorials/how-to-use-irb-to-explore-ruby) knowledge are minimum requirements as all work is done in production Rails console environments.

**Goal**: This mini-module is meant to provide guidance on getting started with GitLab.com console.

**Objectives**: At the end of this module, you should be able to:

- follow console related requests.
- resolve Support Engineer Escalations for GitLab.com through common console related requests.

Start with Stage 1. Stages 2 and 3 can be done simulatenously, **but first**:

1. [ ] After obtaining approval, assign yourself and your manager to this issue.
1. [ ] Open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to request access to `ssh GitLab.com console`.
  1. [ ] Configure a [bastion host](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/bastions/gprd-bastions.md) to SSH into GPRD.
1. [ ] Optional: Set milestones, if applicable, and a due date to help motivate yourself!

### Stage 1: Get primed and ready

- [ ] **Done with Stage 1**

1. [ ] Have an existing Owner add you to the `gitlab-com/support/dotcom/console` group as an `Owner` so that you are a direct member.
1. [ ] Subscribe to the following labels (if not subscribed to all issues in internal requests project):
    1. [Console Escalation::GitLab.com label](https://gitlab.com/gitlab-com/support/internal-requests/-/issues?state=opened&label_name[]=Console%20Escalation%3A%3AGitLab%2Ecom)
    1. [DEWR label](https://gitlab.com/gitlab-com/support/internal-requests/-/issues?label_name%5B%5D=DEWR) (Dotcom Escalation Weekly Report)

### Stage 2: Related reading and resources


- [ ] **Done with Stage 2**

1. Review these resources. Bookmark them to keep them handy:
    1. [ ] [Rails Console Guide](https://docs.gitlab.com/ee/administration/troubleshooting/navigating_gitlab_via_rails_console.html)
    1. [ ] [Rails Console Cheat Sheet](https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html)
1. [ ] Review [when to escalate to infra](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9477) (this currently links to an issue where it's being discussed, and should be updated to a workflow later). Typically, if you're working on an issue and get stuck, reach out to another member first.
1. [ ] GitLab is a much bigger product than customers so there is no diagram. However you can take a look at the [structure.sql file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/structure.sql) paying attention to common pieces like [users](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/structure.sql#L6324), [projects](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/structure.sql#L4983), and [groups](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/structure.sql#L3957).
1. Review these Console Escalation issues to get a better understanding of what is often asked and how to troubleshoot.
    1. [ ] [Force delete project](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1882)
    1. [ ] [Delete uploaded file](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1728)
    1. [ ] [Forcing user confirmation](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1809)
    1. [ ] [More complex batch version of forcing confirmation](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1826)

### Stage 3: Working on issues

- [ ] **Done with Stage 3**

When taking any action, remember to copy and paste your console commands output to record what's been done. If needed, create an issue to record your actions.
Read-only actions don't necessarily need to be recorded, but any change actions do.

1. [ ] Pair on internal issues that require console access. Create a pairing session and link it here.
   1. Pairing:

1. [ ] Answer 3 internal issues that require console access and paste the links here. Remember to paste the commands you run (and output if needed) as a comment in the issue.
   1.
   1.
   1.

### Penultimate Stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list the needed updates below as tasks for yourself!

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Once complete, add this module to the list of training you have completed!

/label ~module
