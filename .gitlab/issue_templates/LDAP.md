---
module-name: "LDAP"
area: "Product Knowledge"
gitlab-group: "Manage:Access"
maintainers:
  - TBD
---

**Title:** _"LDAP - **your-name**"_

**Goal of this checklist:** Set a clear path for LDAP Expert training

Remember to contribute to any documentation that needs updating

### Stage 1: Commit and Become familiar with what LDAP is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Commit to this by notifying the current experts that they can start
routing less-technical LDAP questions to you
1. [ ] Understanding LDAP
   - [ ] Read [What is LDAP-Overview](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
   - [ ] Read [Understanding the LDAP Protocol, Data Hierarchy, and Entry Components](https://www.digitalocean.com/community/tutorials/understanding-the-ldap-protocol-data-hierarchy-and-entry-components)
1. [ ] Using LDAP with GitLab
   - [ ] Watch the [Training Video](https://drive.google.com/open?id=0B9T-nz-7uMATSUxGT3hDX3RKdTg) Note: If needed, [get a license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee)
   - [ ] Watch [How to Manage LDAP, Active Directory in GitLab](https://www.youtube.com/watch?v=HPMjM-14qa8)
   - [ ] Read through all the [LDAP Documentation for CE](https://docs.gitlab.com/ee/administration/auth/ldap.html)
   - [ ] Read through all the [LDAP Documentation for EE](https://docs.gitlab.com/ee/administration/auth/ldap-ee.html) which covers EE specific LDAP features.
   - [ ] Read through how to configure [Google Secure LDAP](https://docs.gitlab.com/ee/administration/auth/google_secure_ldap.html).
   - [ ] Watch the Support Authentication Deep Dive (recorded June 2020) and review the accompanied slides:
      - [ ] [Session 1 of Deep Dive](https://drive.google.com/file/d/16hDb4lHXril_1UmchI5_NlH8iN_b6Kdl/view?usp=sharing)
      - [ ] [Session 2 of Deep Dive](https://drive.google.com/file/d/1nNFX-v1AvCaoibDrcw57jxD59BR-sT8Z/view?usp=sharing)
      - [ ] [Deep Dive Slides](https://docs.google.com/presentation/d/1S8IrmKBLMOsSxEJNQHBLja1ax3qZ0YFicUBM_RFtvVg/edit?usp=sharing)

### Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Set up the GDK with an OpenLDAP server in your dev environment.

   1. [ ] Install the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit)
   1. [ ] Add [OpenLDAP](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/ldap.md) to your GDK installation.
   1. [ ] Try Group sync in the GDK environment. If you don't succeed, contact an
   LDAP expert, then contribute to the documentation on this.
   1. [ ] Run a [UserSync and a GroupSync in the rails console](https://docs.gitlab.com/debug/gitlab_rails_cheat_sheet.html#ldap-commands-in-the-rails-console).

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Go through a few solved LDAP tickets to check the responses and get a sense
of the types of frequently asked questions that come up.
1. [ ] Answer 20 LDAP tickets and paste the links here, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Pair on two Diagnostic calls, where a customer has a problem with LDAP.
   1. [ ] call with ___ on ticket ___
   1. [ ] call with ___ on ticket ___

### Stage 5: Quiz

- [ ] **Done with Stage 5**

1. [ ] Ask your manager for access to the LDAP quiz
1. [ ] Answer the LDAP quiz in a new Google doc: ___
1. [ ] Make the doc private, sharing it only with your manager and all the other LDAP experts (they can be found on the [team page](https://about.gitlab.com/company/team/))

### Final Stage

- [ ] Ask the current LDAP experts to look over this module, your tickets, and your quiz answers. Ask them to comment in this issue that you're either ready to be an expert, or state what else is needed. Check this box once they agree that you're ready.
- [ ] Send a MR to declare yourself an **LDAP Expert** on the team page and assign it to your manager

### Logical Next Steps

1. OmniAuth and SAML: OmniAuth is often used with LDAP and its implementation is similar enough that it's a good idea to learn this next.
  - Setting up [OmniAuth](https://docs.gitlab.com/ee/integration/omniauth.html).
  - Setting up a [SAML](https://docs.gitlab.com/ee/integration/saml.html).
  - [Linking LDAP users](https://docs.gitlab.com/ee/integration/omniauth.html#initial-omniauth-configuration) to an OmniAuth account.
  - Understand the common configuration of using SAML for auth and LDAP groups for GroupSync.

/label ~module
