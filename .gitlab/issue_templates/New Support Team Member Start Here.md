---
module-name: "New Support Team Member Start Here"
area: "Customer Service"
maintainer:
  - vijirao
---

## Introduction

Welcome to the Support Team, we are so excited that you've joined us!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**Your [Onboarding Buddy](https://about.gitlab.com/handbook/support/training/onboarding_buddy.html):** `[tag buddy]`

**Goals of this checklist**

Keep this issue open until you complete the onboarding pathway modules shown below for your role to track your onboarding modules. Once you have completed them and had a discussion with your manager, close this issue.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your onboarding, duration, and other general information.
- Your [Onboarding Buddy](https://about.gitlab.com/handbook/support/training/onboarding_buddy.html) is your primary contact-person during your onboarding. They will schedule regular check-ins with you, and are able to guide you through your [Onboarding Modules](https://about.gitlab.com/handbook/support/training/#support-onboarding-pathway).
- It should take you approximately **5 to 6 weeks to complete** all the modules that make up your onboarding pathway.

1. [ ] Edit this issue's description to remove the irrelevant path based on your role below.


## Support Engineer onboarding pathway

Note: When you've completed the onboarding pathway, speak with your manager and choose your first area of focus (usually "GitLab.com SAAS Support" or "Self-Managed Support")

- Each of the modules listed below have an issue template in our [Support Training project](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates). To create your training issue, create a new issue in the project, in the Description dropdown select the related template, set the title to  "`Your Name`: `module name`", assign it to yourself, and save.

1. New Support Team Member Start Here
1. [ ] Git & GitLab Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Git-GitLab-Basics" target="_blank">create new issue</a>)
1. [ ] Installation & Administration Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=GitLab-Installation-Administration-Basics" target="_blank">create new issue</a>)
1. [ ] GitLab Support Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=GitLab-Support-Basics" target="_blank">create new issue</a>)
1. [ ] Customer Service Skills (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Customer-Service-Skills" target="_blank">create new issue</a>)
1. [ ] Zendesk Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Zendesk-Basics" target="_blank">create new issue</a>)
1. [ ] Working on Tickets (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Working-On-Tickets" target="_blank">create new issue</a>)
1. [ ] Documentation (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Documentation" target="_blank">create new issue</a>) - Completion not required before closing the onboarding issue 


## Support Manager onboarding pathway

Note: Once you complete the below, you may also wish to complete "Installation & Administration Basics" and "Customer Service Skills" from the Support Engineer onboarding pathway.

1. New Support Team Member Start Here
1. [ ] Support Manager Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Support-Manager-Basics" target="_blank">create new issue</a>)
1. [ ] Git & GitLab Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Git-GitLab-Basics" target="_blank">create new issue</a>)
1. [ ] GitLab Support Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=GitLab-Support-Basics" target="_blank">create new issue</a>)
1. [ ] Zendesk Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Zendesk-Basics" target="_blank">create new issue</a>)
1. [ ] Customer Emergencies (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Customer-Emergencies" target="_blank">create new issue</a>)
1. [ ] GitLab.com CMOC (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=GitLab-com-CMOC" target="_blank">create new issue</a>)
1. [ ] Booster - Dotcom Emergency Training (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Booster%20-%20Dotcom%20Emergency%20Training" target="_blank">create new issue</a>)
1. [ ] SSAT Reviewing Manager (<a href="https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=SSAT-Reviewing-Manager" target="_blank">create new issue</a>)

## Final Steps, Onboarding Feedback & Documentation

1. [ ] Have a chat with your manager about getting yourself added to the [Support Team Sync meeting rotation as Chair and Note-taker](https://docs.google.com/document/d/1jwj5g0BIq3kTepw2-ZD9VSETs7Isf6YDHGzmYxmTt50/edit#heading=h.tyva1uqh3g20).
    Note: This is not required for team members in the US as the format for team calls is currently different there.

Keeping our documentation and workflows up to date will ensure that all Support team members will be able to access and learn from the best practices of the past. Giving feedback about your onboarding experience will ensure that this document is always up to date, and those coming after you will have an easier time coming in.

1. [ ] Schedule a call with your manager to discuss your onboarding issue or integrate this into your 1:1. Answer the following questions:
    - What was most helpful?
    - What do you wish existed in your onboarding, but does not?
    - In which areas do you still not feel confident?
    - In which areas do you feel strong?
  1. [ ] Make an update to one or more Support training module templates to make it better and link them below. The files are located in an issue template in the [support-training repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).
    1. _________

#### Congratulations on completing your Support Onboarding modules successfully!

/label ~onboarding
