---
module-name: "GitLab Support Basics"
area: "Customer Service"
maintainers:
  - vijirao
---

## Introduction

Welcome to the next module in your Support Onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**Goals of this checklist**

At the end of the checklist, new team member should
- Be familiar with how GitLab Support operates
- Be familiar with some common Support Workflows and Processes
- Be familiar with issue creation and escalation

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/), the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **1 day to complete**.

### Stage 0. GitLab Support Basics

1. [ ] Get familiar with our [Customer facing Support](https://about.gitlab.com/support/) page and [Statement of Support](https://about.gitlab.com/support/statement-of-support.html).
1. [ ] Read and bookmark the [Support Engineer Responsibilities page](https://about.gitlab.com/handbook/support/support-engineer-responsibilities.html)
1. [ ] Familiarize yourself with the [Support Workflows](https://about.gitlab.com/handbook/support/workflows/) handbook page. Bookmark this page and use it as a reference, rather than trying to read through all the workflows right now.
1. [ ] See how our Support Engineers handle tickets by reading and understanding the [Working on Tickets](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html) workflow.
1. [ ] Learn about the [roles our Support Engineers rotate in](https://about.gitlab.com/handbook/support/workflows/meeting-service-level-objectives.html), so that people know what to do and have variation in their work.
1. [ ] Read about the [Support Channels](https://about.gitlab.com/handbook/support/channels/) and how customers on paid plan vs free plan receive Support.
   - Here you will find tickets from both paid customers and free/trial users.
   - Tickets are extremely varied.
   - You should be prepared for basic tickets given the knowledge gained from the previous steps of your training.
   - Tickets in Zendesk contain a wealth of knowledge from past interactions. Often times, the issue has occurred at least once and a solution has been provided, or an issue created on the customer's behalf. Reading through past tickets can aide in the learning process, getting you up to speed quicker.
1. [ ] Get to know the GitLab [Support Bot](https://about.gitlab.com/handbook/support/#support-slackbot). Search for the `supportbot` in the Apps section on Slack, and type "sb sh" (self-managed) or "sb dc" (GitLab.com). This will return a list of the number of active tickets by type. 
    Note that you can get the same results communicating with the support bot in any of the Support Slack channels as well.  
1. [ ] Review the [gitlab.io Support Team page](https://gitlab-com.gitlab.io/support/team/), which gives up-to-date information on team members and schedules.
   1. [ ] Create an MR to add yourself to the `support-team.yaml` file with your own information. Assign to your manager for review and merge.

### Stage 1: Creating issues and Fielding Feature Proposals**

1. [ ] Understand what's in the pipeline at GitLab as well as proposed features: [Direction Page](https://about.gitlab.com/direction/).
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/groups/gitlab-org/-/labels) to find existing feature proposals and bugs.
1. [ ] If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk.
1. [ ] Add [customer labels](https://about.gitlab.com/handbook/support/workflows/working-with-issues.html#adding-labels) for those issues relevant to our subscribers.
1. [ ] Take a look at the [existing issue templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/.gitlab/issue_templates) to see what is expected (look at the comments in the markup for details.). Raise issues for bugs in a manner that would make the issue easily reproducible. A Developer or a contributor may work on your issue.

### Stage 2. Issue Escalation Process

Some tickets need specific knowledge or a deep understanding of a particular component and will need to be escalated to a Senior Support Engineer or a Developer.

1. [ ] Read about [creating and escalating issues](https://about.gitlab.com/handbook/support/workflows/working-with-issues.html) including how issues are priotized.
1. [ ] Take a look at the [Team page](https://about.gitlab.com/team/) to find the resident experts in their fields and the [knowledge areas page](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html) for further topics where team members are keen on helping with specific areas.
1. [ ] Add yourself to the [knowledge areas page](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html) for topics you may know already from previous experience.

_Note: If you're interested in making a contribution to the code, please see the [GitLab development guidelines](https://docs.gitlab.com/ee/development/)._

### Stage 3. Resources for Getting Help

1. [ ] To ask for help, post in the appropriate Slack channel and add a link as an internal note in the ticket, if applicable.
    - #questions for general GitLab questions
    - #support_gitlab-com or #support_self-managed for ticket (or issue) specific questions
    - Appropriate #s_ (stage) or #g_ (group) channel. Broken up according to the [product categories page](https://about.gitlab.com/handbook/product/categories/#devops-stages).
1. [ ] When you get no response or need someone more urgently, you can ping the appropriate [support counterpart](https://about.gitlab.com/handbook/support/#support-stable-counterparts) or someone with [knowledge in the area](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html).
1. [ ] If someone asks you for help, whether directed at the team or you individually, consider these [Slack reactions tips](https://about.gitlab.com/handbook/support/workflows/communication-tips.html#slack-reactions).
1. [ ] If you need to bring something to the team's attention but don't want to ping people, consider using [mustread Slack app](https://about.gitlab.com/handbook/support/workflows/communication-tips.html#mustread)

## Congratulations! You made it, and now have a basic understanding of how GitLab Support operates and the most common workflows we have!

You are now ready to continue on your onboarding path to tackle the next module in line. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [training page](https://about.gitlab.com/handbook/support/training/) or your `New Team Member Start Here` issue to see the list of all modules under your Onboarding pathway!

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
