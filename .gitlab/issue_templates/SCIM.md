**Title:** _"SCIM Module - **your-name**"_

**Goal of this checklist:** Set a clear path for SCIM Expert training

Before tackling this module, you will need to first complete the SAML module. Remember to contribute to any documentation that needs updating.

**Objectives**: At the end of this module, you should be able to:
* Understand how to set up SAML and SCIM apps for SSO for Groups
* Troubleshoot customer's issues with SAML and SCIM

### Stage 1: Commit and Become familiar with what SCIM is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started.
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical SCIM questions to you
1. [ ] Read through the [GitLab SCIM provisioning using SAML SSO for Groups Documentation](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html).
1. [ ] Watch the [Support SCIM Deep Dive](https://youtu.be/4OxDhtR_WYM) (recorded December 2020), with optional [APAC Q&A](https://youtu.be/SUegvRltdCA) and [EMEA Q&A](https://youtu.be/VS8oRwfNfrQ) sessions. You can also [access the SCIM Deep Dive slides](https://docs.google.com/presentation/d/1ZceIBJFuWzWMtRMsCSVnoKOHbVFo4TWHegKdNVacD8g/edit).

### Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Create a test app on a cloud provider IdP where we support SCIM and connect it either with your GDK or a GitLab.com group.
    1. [ ] The [infrastructre for troubleshooting workflow page](https://about.gitlab.com/handbook/support/workflows/test_env.html#azure-testing-environment) has info on getting access to Azure or Okta. Alternatively, you can create a trial account on one of the platforms. As most questions are about Azure, consider choosing Azure for this exercise.
    1. [ ] If using GitLab.com, ensure your GitLab.com test group has Silver or Gold plan. If needed, create an access request to get a test group upgraded, or ask team members for access to an existing one.
    1. [ ] Follow the documentation to set up SAML and SCIM. The [specific provider sections](https://docs.gitlab.com/ee/user/group/saml_sso/#providers) link to a demo video if one exists. If we have SCIM for an IdP where no such video exists, consider contributing one!
    1. [ ] Ensure you are familiar with the [troubleshooting section](https://docs.gitlab.com/ee/user/group/saml_sso/#troubleshooting) in order to know what common cases are documented.

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Go through 5 solved SCIM-related tickets to check the responses and get a sense
of the types of frequently asked questions that come up.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 SCIM-related tickets and paste the links here, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Penultimate Stage: Review
You feel that you can now do all of the objectives:
1. [ ] Understand how to set up SAML and SCIM apps for SSO for Groups.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself!
* [ ] Update ...

### Final Stage

- [ ] Have your trainer and manager review this issue.
- [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
- [ ] Send a MR to declare yourself a **SCIM Expert** on the team page
