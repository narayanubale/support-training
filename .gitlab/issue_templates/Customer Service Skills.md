---
module-name: "Customer Service Skills"
area: "Customer Service"
maintainers:
  - vijirao
---

## Introduction

Welcome to the next module in your Support Onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**Goals of this checklist**

At the end of the checklist you will be able to:
- understand how we interact with customers
- utilize your customer service skills to ensure customer success

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/#support-onboarding-pathway), the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **1 day to complete**.

### Stage 0. Customer Service at GitLab Support

1. [ ] Read the `We care for our customers` section on the [support team's home page](https://about.gitlab.com/handbook/support/#we-care-for-our-customers) in the handbook to understand what we are responsible for.
1. [ ] Read [how to respond to tickets](https://about.gitlab.com/handbook/support/workflows/how-to-respond-to-tickets.html) on tips and tricks to humanize your responses when talking to customers.
1. [ ] Read about how to handle [feedback and complaints](https://about.gitlab.com/handbook/support/workflows/feedbacks_and_complaints.html) from customers.

### Stage 1. Customer Service Skills Resources

_Course creation in progress_

#### Congratulations! You made it, and now have a basic understanding of how we approach customer service and management at GitLab!

You are now ready to continue on your onboarding path to tackle the next module in line based on your role. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [onboarding page](https://about.gitlab.com/handbook/support/onboarding/) or your `New Support Team Member Start Here` issue to see the list of all modules under the Onboarding pathway!

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
