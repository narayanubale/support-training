---
module-name: "GitLab Intermediate Topics"
area: "Product Knowledge"
maintainers:
  - vijirao
---

## Introduction

After completing the core Support Engineering training, please discuss with your manager whether to explore any intermediate-level topics.

**Goals of this module**

At the end of this module, you will:
- understand one or more of the listed features from both an end-user perspective and an administrative perspective.

**General Timeline and Expectations** 

- Work with your manager to plan your progress.

## Intermediate GitLab Topics

This section describes multiple options to explore. Do not do all of them, because they might not be relevant to what customers need right now, and can be a significant time investment.

For each topic below, you can use GitLab.com to understand the features from an end-user perspective, then set up a test instance to understand the configuration of the feature from an Administrative perspective.

- [ ] [GitLab API](https://docs.gitlab.com/ee/api/README.html)
- [ ] [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)
- [ ] [GitLab Pages](https://docs.gitlab.com/ee/administration/pages/index.html)
- [ ] [GitLab through the Rails Console](https://docs.gitlab.com/ee/administration/troubleshooting/navigating_gitlab_via_rails_console.html)
- [ ] [Advanced GitLab debugging and log collection](/gitlab-com/support/support-team-meta/-/issues/1900#5-try-something-a-little-more-advanced)

## Further Exploration

Peruse the [Support Learning repository](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates) for additional Learning Modules to explore.

/label ~onboarding
