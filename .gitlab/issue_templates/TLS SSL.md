---
module-name: "TLS SSL"
area: "Core Technologies"
maintainers:
  - TBD
---

**Title:*- _"Gitlab TLS - **your-name**"_

### Overview

**Goal**: Set a clear path for GitLab TLS / SSL expert training

**Objectives**: At the end of this module, you should be able to:

- Understand the basics of how SSL works.
- Understand how GitLab interacts with SSL.
- Feel comfortable troubleshooting GitLab SSL issues.

### Stage 1: Commit and become familiar with how SSL works

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Gitlab SSL questions to you
1. [ ] Read SSL Documentation
  - [ ] Read [What is SSL?](https://www.ssl.com/faqs/faq-what-is-ssl/)
  - [ ] Read [What is an SSL certificate?](https://www.digicert.com/ssl/)
  - [ ] Read [What is a certificate authority?](https://www.ssl.com/faqs/what-is-a-certificate-authority/)
  - [ ] Read [What is the difference between a self-signed certificate and a trusted CA signed certificate?](https://cheapsslsecurity.com/blog/self-signed-ssl-versus-trusted-ca-signed-ssl-certificate/)
1. [ ] Read GitLab Documentation
  - [ ] Read [NGINX settings](https://docs.gitlab.com/omnibus/settings/nginx.html)
  - [ ] Read [SSL Configuration](https://docs.gitlab.com/omnibus/settings/ssl.html)
  - [ ] Read [Runner SSL documentation](https://docs.gitlab.com/runner/configuration/tls-self-signed.html)

### Stage 2: Technical setup

- [ ] **Done with Stage 2**

1. [ ] Configure SSL for GitLab using the Let'sEncrypt integration.
1. [ ] Configure SSL for GitLab using a self-signed certificate.
1. [ ] Configure GitLab to trust a self-signed certificate.
1. [ ] Configure GitLab to trust a certificate chain.
1. [ ] Configure a Runner to trust a self-signed certificate.
1. [ ] Configure a Runner to trust a certificate chian.

### Stage 3: Working with GitLab and SSL

- [ ] **Done with Stage 3**

Remember to contribute to any documentation that needs updating.

1. [ ] Look for 10 old SSL tickets and read through them to understand what the
issues were and how they were addressed. Paste the links here.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Look for 5 SSL related issues on gitlab-org issue tracker, read through them, and
paste the links here

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Answer 10 SSL tickets and paste the links here. Do this even if a ticket
seems too advanced for you to answer. Find the answers from an expert and relay
them to the customers.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Pair on two diagnostic calls, where a customer is having trouble with SSL.
   1. [ ] call with ___
   1. [ ] call with ___

### Stage 5: Quiz

- [ ] **Done with Stage 5**

1. [ ] Request access to the [SSL Module Quiz](https://docs.google.com/document/d/1TeYNB1LhtZpmHrlyskPxGJU1M5Ix0_BTrviAbcXXp-o/edit?usp=sharing) from your manager or a GitLab SSL Expert
1. [ ] Create a copy of the quiz to supply your answers on (Do not add answers to shared quiz)
1. [ ] Quiz answers were checked by a GitLab SSL Expert and they said you passed

### Penultimate Stage: Review

You feel that you can now do all of the objectives:

- Have a basic understanding of how SSL works.
- Have a basic understanding of how GitLab interacts with SSL.
- Feel comfortable troubleshooting GitLab SSL issues.

- ### Final Stage:

- [ ] Your Manager needs to check this box to acknowledge that you finished
- [ ] Send a MR to declare yourself a GitLab SSL expert on the team page

/label module
