# Seed your Test Instance and Learn the GitLab API!

**A note about emails** - if your mail server is configured to send outgoing mail to the internet, it is recommended that you disable outgoing GitLab emails temporarily before you begin seeding to prevent a barrage of emails being sent out. In your `/etc/gitlab/gitlab.rb` file, you can set `gitlab_rails['gitlab_email_enabled']` to `false` and then run `sudo gitlab-ctl reconfigure` for the changes to take effect.

### Utilize the GitLab API to add test data to your Omnibus installation via the [gitlab gem](https://rubygems.org/gems/gitlab/versions/3.6.1). In addition, use the [faker gem](https://rubygems.org/gems/faker) to generate fun test data.

1. [ ] Get an access token for your GitLab instance by going to your user settings -> Access Tokens. Generate one that has the `api` scope.
2. [ ] Install dependencies:
```bash
$ gem install gitlab faker
```
3. [ ] Create a file named `seed_data.rb`. Using the gitlab gem, define the API client to communicate with:
```ruby
require 'gitlab'
client = Gitlab.client(
  endpoint: 'https://yourinstance.gitlab.com/api/v4',
  private_token: '***************'
)
```
4. [ ] Utilizing the Faker gem, build out groups, projects, people, and descriptions that you'll use to create data on your GitLab instance. Be creative! Explore the gem so you can make the data your own :)
```ruby
require 'faker'
def gen_groups
  [
    Faker::TvShows::GameOfThrones.house,
    Faker::Food.dish,
    Faker::TvShows::ParksAndRec.city,
    Faker::TvShows::SiliconValley.company
  ].sample.gsub(/[^0-9A-Za-z]/, '')
end

def gen_projects
  [
    Faker::App.name,
    Faker::Food.spice,
    Faker::TvShows::SiliconValley.app,
    Faker::University.name
  ].sample.downcase.gsub(/[^0-9A-Za-z]/, '')
end

def gen_people
  [
    Faker::Movies::BackToTheFuture.character,
    Faker::TvShows::HowIMetYourMother.character,
    Faker::TvShows::GameOfThrones.character,
    Faker::TvShows::ParksAndRec.character
  ].sample
end

def gen_description
  [
    Faker::Hacker.say_something_smart,
    Faker::Company.bs
  ].sample
end
```
5. [ ] Define the amount of users and groups to create, and generate them with the Faker data:
```ruby
user_count = 55
group_count = 15
people = Array.new(user_count) { gen_people }.uniq
groups = Array.new(group_count) { gen_groups }.uniq
```
6. [ ] Generate users and groups through the [`create_user`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/Users#create_user-instance_method) and [`create_group`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/Groups#create_group-instance_method) gitlab gem methods:
```ruby
users = people.map do |user|
  username = user.downcase.gsub(/[^0-9A-Za-z]/, '')
  email = "#{username}@blair.cloud" #consider replacing with your domain
  password = 'blairblair' #consider changing or randomly generating passwords
  puts "User -- Name: #{user}, UserName: #{username}, Email: #{email}"
  client.create_user(email, password, username, name: user)
end
groups = groups.map do |group|
  path = group.downcase.gsub(/[^0-9A-Za-z]/, '')
  puts "Group -- #{group}/#{path}"
  client.create_group(group, path)
end
```
7. [ ] Add users to each group with various access levels with the [`add_group_member`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/Groups#add_group_member-instance_method) gitlab gem method:
```ruby
group_access = [10, 20, 30, 40, 50]
groups.each do |group|
  users.sample(rand(1..users.count)).each do |user|
    begin
      puts "Group Add: #{group.name}: #{user.name}"
      client.add_group_member(group.id, user.id, group_access.sample)
    rescue StandardError
      next
    end
  end
end
```
8. [ ] Add projects to groups, using a range of projects per group, with the [`create_project`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/Projects#create_project-instance_method) gitlab gem method:
```ruby
project_range = 2..10
groups.each do |group|
  project_names = Array.new(rand(project_range)) { gen_projects }
  project_names.uniq.each do |project|
    puts "Project: #{project}"
    options = {
      description: gen_description,
      default_branch: 'master',
      issues_enabled: 1,
      wiki_enabled: 1,
      merge_requests_enabled: 1,
      namespace_id: group.id
    }
    client.create_project(project, options)
  end
end
```
9. [ ] Populate projects with issues using the [`create_issue`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/Issues#create_issue-instance_method) gitlab gem method:
```ruby
client.projects.auto_paginate .each do |project|
  group = client.group(project.to_h.dig('namespace', 'id'))
  members = client.group_members(group.id).auto_paginate
  rand(5..40).times do
    options = {
      description: Faker::Hacker.say_something_smart,
      assignee_id: members.sample.id
    }
    client.create_issue(project.id, Faker::Company.catch_phrase, options)
    puts 'Issue Created'
  end
end
```
10. [ ] Put all of these into the ruby document, `seed_data.rb`, and run the following (you may need to download ruby if you don't have it installed):
```
ruby seed_data.rb
```

### Additional Information:

Potential other seeding methods:
- Labels with the [`create_label`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/Labels#create_label-instance_method) gitlab gem method
- Merge Requests with the [`create_merge_requests`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/MergeRequests#create_merge_request-instance_method) gitlab gem method
- Milestones with the [`create_milestone`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/Milestones#create_milestone-instance_method) gitlab gem method
- Repository Files with the [`create_file`](https://www.rubydoc.info/gems/gitlab/3.6.1/Gitlab/Client/RepositoryFiles#create_file-instance_method) gitlab gem method


### To Do

- Further testing, expand on explanation, additional seeding with further GitLab gem methods. 
