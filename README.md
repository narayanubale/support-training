# Support Team Trainings

Welcome to the GitLab Support team trainings project

We use this space for:

- Support onboarding issues
- trainings / modules
- interview training

Pairing issue should be created in the [Support Pairing project](https://gitlab.com/gitlab-com/support/support-pairing/issues)

Any issues not related to the items listed should go in the [support issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues).

Support team resources:
 - Handbook: https://about.gitlab.com/handbook/support/
 - Support Workflows: https://about.gitlab.com/handbook/support/workflows/
 - GitLab docs: http://docs.gitlab.com/

For a list of available modules, please check the list of [issue templates](/.gitlab/issue_templates/).
